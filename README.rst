.. SPDX-FileCopyrightText: 2023 Linutronix GmbH
.. SPDX-License-Identifier: CC0-1.0

About
=====

This is the `x86-cpuid.org`_ Huge-based website repository.

In the future, we would also like the website to have a list the CPUID
bitfields, etc.

.. _x86-cpuid.org: https://x86-cpuid.org
